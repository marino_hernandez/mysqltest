
DROP DATABASE IF EXISTS logtest;
CREATE DATABASE logtest;

DROP table IF EXISTS log ;

create table log(
  id bigint(20) NOT NULL AUTO_INCREMENT  PRIMARY KEY,
  date_log datetime NOT NULL,
  ip varchar(255) DEFAULT NULL,
  method varchar(255) DEFAULT NULL,
  status_code varchar(255) DEFAULT NULL,
  user_agent varchar(255) DEFAULT NULL  
);

DROP table IF EXISTS log_comments ;

create table log_comments(
  id bigint(20) NOT NULL AUTO_INCREMENT  PRIMARY KEY, 
  ip varchar(255) DEFAULT NULL,
  number_of_request int DEFAULT NULL,
  comment varchar(255) DEFAULT NULL
  
);