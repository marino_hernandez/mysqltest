/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author edwin
 */
public class ConnectionFactory {
    
    public static Connection getConnection(){
        try{
             Class.forName("com.mysql.cj.jdbc.Driver");
             Locale defaultLocale = new Locale("en", "US");
             ResourceBundle rb = ResourceBundle.getBundle("mysqlproperties",defaultLocale);
             return    DriverManager.getConnection(rb.getString("url"),
                     rb.getString("user"),rb.getString("pass"));
        }
        catch(SQLException  | ClassNotFoundException e){
            throw new RuntimeException("Error connecting to the database", e);
        }
       
    }
}
