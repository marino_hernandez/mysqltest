/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.dao;

import com.ef.model.LogComment;
import com.ef.model.LogDto;
import java.util.List;

/**
 *
 * @author edwin
 */
public interface LogDao {
    
    void createLog(List<LogDto> logs);
    
    void createLogComments(List<LogComment> logs);
    
    List<LogComment> findIpsByTimePeriod(String startDate, String duration, String threshold);
    
      
    
}
