/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.logparser;

import com.ef.model.LogComment;
import com.ef.service.LogService;
import com.ef.model.LogDto;
import com.ef.util.EfDateUtil;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author edwin
 */
public class LogParser {

    public void parseLog(String path, String startDate, String duration, String treshold) throws IOException, ParseException {

        Stream<String> logLines = null;
        logLines = Files.lines(Paths.get(path));
        final long initialDate = EfDateUtil.formatDateYYYYMMddHHmmss(startDate).getTime();
        final long finalDate = EfDateUtil.calculateEndDate(initialDate, duration);
        List<LogDto> log
                = logLines.map(line -> line.split("\\|"))
                        .map(this::createLog)
                        .filter(logDto -> {
                            return filterLog(logDto, initialDate, finalDate);

                        })
                        .collect(Collectors.toList());
        LogService.getInstance().createLog(log);
        List<LogComment> logs = LogService.getInstance().
                findIpsByTimePeriod(startDate, duration, treshold);
        if (!logs.isEmpty()) {
            LogService.getInstance().createLogComments(logs);
            logs.stream().forEach(logMoreRequest -> {

                Logger.getLogger(LogParser.class.getName()).log(Level.INFO, logMoreRequest.getIp());
            });
        }

    }

    public boolean filterLog(LogDto log, long initialDate, long finalDate) {
        if (log.getDate().getTime() >= initialDate && log.getDate().getTime() <= finalDate) {
            return true;
        }
        return false;
    }

    public LogDto createLog(String[] line) {

        return new LogDto(EfDateUtil.
                formatDateYYYYMMddHHmmssSS(line[0]), line[1], line[2], line[3], line[4]);

    }
}
