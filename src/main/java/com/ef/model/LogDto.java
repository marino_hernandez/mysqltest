/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author edwin
 */
public class LogDto implements Serializable{
    
    private Date date;
    private String ip;
    private String method;
    private String statusCode;
    private String userAgent;

    public LogDto(Date date, String ip, String method, String statusCode, String userAgent) {
        this.date = date;
        this.ip = ip;
        this.method = method;
        this.statusCode = statusCode;
        this.userAgent = userAgent;
    }
    
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    
    
    
    
    
}
