/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.model;

import java.io.Serializable;

/**
 *
 * @author edwin
 */
public class LogComment implements Serializable{
    private String ip;
    private Integer numbeOfRequest;
    private String comment;

    public LogComment(String ip, Integer numbeOfRequest, String comment) {
        this.ip = ip;
        this.numbeOfRequest = numbeOfRequest;
        this.comment = comment;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getNumbeOfRequest() {
        return numbeOfRequest;
    }

    public void setNumbeOfRequest(Integer numbeOfRequest) {
        this.numbeOfRequest = numbeOfRequest;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    
}
