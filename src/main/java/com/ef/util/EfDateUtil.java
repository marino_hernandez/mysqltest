/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class EfDateUtil {
    
    private static final DateFormat FORMAT1;
    private static final DateFormat FORMAT2;
    private static final String DAILY ="daily";
    private static final String HOURLY ="hourly";
    
    static{
       FORMAT1 = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss"); 
       FORMAT2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
    }
    
    public static Date formatDateYYYYMMddHHmmss(String date){
        try {
            return FORMAT1.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(EfDateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static Date formatDateYYYYMMddHHmmssSS(String date){
        try {
            return FORMAT2.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(EfDateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
     public static long calculateEndDate(long initialDate, String duration) {
        if (duration.equals(DAILY)) {
            return initialDate + (3600000 * 24);
        } else if (duration.equals(HOURLY)) {
            return initialDate + (3600000);
        }
        return 0;
    }
    
    
}
