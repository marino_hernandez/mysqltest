/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import com.ef.logparser.LogParser;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class Parser {
    
    
    public static void main(String[] args) throws IOException, ParseException {
        System.out.println(args.length);
        if(args.length == 4){
            Logger.getLogger("Parser").info("OK");
            LogParser parser = new LogParser();
            parser.parseLog(args[0].split("=")[1],args[1].split("=")[1],args[2].split("=")[1],
                    args[3].split("=")[1]);
        } else{
            Logger.getLogger(Parser.class.getName()).info("Invalid Parameters");
        }
    }
}
