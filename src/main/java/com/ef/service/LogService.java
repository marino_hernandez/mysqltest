/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.service;

import com.ef.dao.LogDao;
import com.ef.model.LogComment;
import com.ef.model.LogDto;
import com.ef.sql.ConnectionFactory;
import com.ef.util.EfDateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class LogService implements LogDao {
    
    private static LogService service = null;
    
    public static synchronized LogService getInstance(){
        if(service == null)
            service = new LogService();
        return service;
    }

    @Override
    public void createLog(List<LogDto> logs) {

        try {
           
            Connection con = ConnectionFactory.getConnection();
            String sql = "insert into log(date_log, ip, method, status_code, user_agent) values (?,?,?,?,?)";

            PreparedStatement stm = con.prepareStatement(sql);
            final int batchSize = 5000;
            int count = 0;
            for (LogDto log : logs) {
                stm.setTimestamp(1, new Timestamp(log.getDate().getTime()));
                stm.setString(2, log.getIp());
                stm.setString(3, log.getMethod());
                stm.setString(4, log.getStatusCode());
                stm.setString(5, log.getUserAgent());
                stm.addBatch();
                if (++count % batchSize == 0) {
                    stm.executeBatch();
                }
            }
            stm.executeBatch();
           
            stm.close();
            con.close();
            Logger.getLogger(LogService.class.getName()).log(Level.INFO, "Log process sucessfully");
        } catch (SQLException ex) {
            Logger.getLogger(LogService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void createLogComments(List<LogComment> logs) {
        try {
           
            Connection con = ConnectionFactory.getConnection();
            String sql = "insert into log_comments(ip, number_of_request, comment) values (?,?,?)";

            PreparedStatement stm = con.prepareStatement(sql);
            final int batchSize = 5000;
            int count = 0;
            for (LogComment log : logs) {               
                stm.setString(1, log.getIp());
                stm.setInt(2, log.getNumbeOfRequest());
                stm.setString(3, log.getComment());               
                stm.addBatch();
                if (++count % batchSize == 0) {
                    stm.executeBatch();
                }
            }
            stm.executeBatch();           
            stm.close();
            con.close();
            Logger.getLogger(LogService.class.getName()).log(Level.INFO, "Log process sucessfully");
        } catch (SQLException ex) {
            Logger.getLogger(LogService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<LogComment> findIpsByTimePeriod(String startDate, String duration, String threshold) {
        List<LogComment> result = new ArrayList<>();
        try {
            String query = "SELECT count(log.ip), log.ip FROM log log  WHERE log.date_log BETWEEN ? AND ? GROUP BY log.ip HAVING count(log.ip) >= ?";
            final long initialDate = EfDateUtil.formatDateYYYYMMddHHmmss(startDate).getTime();
            final long finalDate = EfDateUtil.calculateEndDate(initialDate, duration);
            Connection con = ConnectionFactory.getConnection();
            PreparedStatement stm = con.prepareStatement(query);
            stm.setTimestamp(1,new Timestamp(initialDate) );
            stm.setTimestamp(2,new Timestamp(finalDate) );
            stm.setInt(3, Integer.parseInt(threshold));
            ResultSet rs = stm.executeQuery();           
            while(rs.next()){
               LogComment dto = new LogComment(rs.getString(2), rs.getInt(1), "more than "+threshold +"request");
               result.add(dto);
            }
            stm.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(LogService.class.getName()).log(Level.SEVERE, null, ex);
        }
          return result;
    }

   

}
